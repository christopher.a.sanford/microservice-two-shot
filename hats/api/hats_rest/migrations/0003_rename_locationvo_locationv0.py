# Generated by Django 4.0.3 on 2024-01-31 02:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_rename_locationv0_locationvo'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='LocationVO',
            new_name='LocationV0',
        ),
    ]
