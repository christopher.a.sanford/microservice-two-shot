from django.db import models
from django.urls import reverse


class LocationV0(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href=models.CharField(max_length=200, unique=True)



class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200, default='green')
    picture_url = models.URLField(null=True)
    
    location = models.ForeignKey(
        LocationV0, 
        related_name='hats', 
        on_delete=models.CASCADE
        )

    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})


    

    
    





# Create your models here.
