from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationV0
from django.http import JsonResponse
import json



# Create your views here.

from django.views.decorators.http import require_http_methods



class LocationV0DetailEncoder(ModelEncoder):
    model = LocationV0
    properties = [
        'closet_name'
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style',
        'picture_url',
        'color',
        'location',
        'id',
        
        
    
    ]

    encoders = {
        'location': LocationV0DetailEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style',
        'picture_url',
        'color',
        'location',
        'id',
        
        
        
    ]

    encoders = {
        'location': LocationV0DetailEncoder(),
    }

    





@require_http_methods(['GET', 'POST'])
def api_list_hats(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
            location_href = content['location']
            
            location = LocationV0.objects.get(import_href=location_href)
            
            content['location'] = location
        except LocationV0.DoesNotExist as e: 

            return JsonResponse(
                {'message': 'Invalid location id', 'error': str(e)}, 
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'DELETE'])
def api_show_hat(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {'hat': hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})



