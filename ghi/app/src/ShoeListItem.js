import React from 'react';

const ShoeListItem = function({ shoe, deleteShoe }) {

    return(
        <div className="card w-25 m-3">
            <img src={shoe.picture_url} className="card-img-top" />
            <div className="card-body">
                <h6 className="card-title">{shoe.manufacturer} - {shoe.model_name}</h6>
                <div>Color: {shoe.color}</div>
                <div>Location: {shoe.bin.id}</div>
                <button onClick={() => deleteShoe(shoe.id)} className="btn btn-lg btn-danger w-100 mt-3">Delete</button>
            </div>
        </div>
    )
}

export default ShoeListItem;
