import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm'
import ShoeForm from "./ShoeForm";
import ShoeList from "./ShoeList";
import Hats from './Hats'


function App(props) {
  if (props.hats === undefined) {
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='hats'>
            <Route path='' element={<Hats hats={props.hats} />} />
            <Route path='new' element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
