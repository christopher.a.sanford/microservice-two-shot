import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';








function Hats (props) {

    const [hats, setHats] = useState([]);

    const handleDelete = async (id) => {
        try {
            const response = await fetch(
                `http://localhost:8090/api/hats/${id}`, 
                { method: 'DELETE' }
            )

            if (response.ok) {
                const updatedHats = hats.filter(hat => hat.id !== id);
                setHats(updatedHats);
                

            } else {
                console.error('Failed to delete hat');
            }
        } catch (error) {
            console.error('error deleting hat', error);
        }
    }

    const fetchHats = async () => {
        try {

            const url = 'http://localhost:8090/api/hats';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setHats(data.hats);
                
            } else {
                throw new Error('failed to fetch');
            }
        } catch (error) {
            console.error('error:', error);
        }
    };


    useEffect(() => {
        fetchHats()
    }, []);

    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={ hat.id }>
                            <td>{ hat.style }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location.closet_name }</td>

                            <td><button onClick={() => handleDelete(hat.id)} id='delete' className='btn btn-danger btn-block'>Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default Hats