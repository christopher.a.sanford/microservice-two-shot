import React, { useEffect, useState } from 'react';

function HatForm() {
    const [style, setStyle] = useState('');

    const [color, setColor] = useState('');

    const [fabric, setFabric] = useState('');

    const [pictureUrl, setPictureUrl] = useState('');

    //set state of location

    const [location, setLocation] = useState('');

    //set state of location list

    const [locations, setLocations] = useState([]);

    //event handlers

    const handleStyleChange = (e) => {
        const value = e.target.value;
        setStyle(value);
    };

    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    };

    const handleFabricChange= (e) => {
        const value = e.target.value;
        setFabric(value);
    };

    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    };

    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    };

    //handleSubmit

    const handleSubmit = async (e) => {
        e.preventDefault();

        //create empty json object

        const data = {};

        data.style = style;
        data.color = color;
        data.fabric = fabric;
        data.picture_url = pictureUrl;
        data.location = location;


        const hatUrl = `http://localhost:8090/api/hats/`

        const fetchConfig = {
            method: 'post', 
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);

        console.log(response);

        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setStyle('');
            setColor('');
            setFabric('');
            setPictureUrl('');
            setLocation('');

        }






    };

    //fetch all locations

    

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            console.log(data);
            console.log(locations)
            setLocations(data.locations);
            
        }
    }

    //useEffect

    useEffect(() => {
        fetchData()
    }, [])

    return(
        <main>
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Add a New Hat</h1>
                            <form onSubmit={handleSubmit} id="create-hat-form">
                                <div className="form-floating mb-3">
                                    <input onChange={handleStyleChange} placeholder="Style" required type="text" name="style" value={style} id="style" className="form-control" />
                                    <label htmlFor="presenter_name" >Style</label>
                                </div>


                                <div className="form-floating mb-3">
                                    <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" value={color} id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>
                                </div>


                                <div className="form-floating mb-3">
                                    <input onChange={handleFabricChange} placeholder="Fabric" type="text" name="fabric" id="fabric" value={fabric} className="form-control" />
                                    <label htmlFor="fabric">Company name</label>
                                </div>


                                <div className="form-floating mb-3">
                                    <input onChange={handlePictureUrlChange} placeholder="Picture Url" required type="text" name="url" id="url" value={pictureUrl} className="form-control" />
                                    <label htmlFor="title">Picture Url</label>
                                </div>
                                
                                <div className="mb-3">
                                    <select onChange={handleLocationChange} required id="location" name="location"  value={location} className="form-select">
                                        <option value="">Choose a Location</option>
                                        {locations.map(location => {
                                            return (
                                                <option key={location.href} value={location.href}>
                                                    {location.closet_name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )

    



    
}

export default HatForm
