import React, { useState, useEffect } from 'react';
import ShoeListItem from './ShoeListItem';

const ShoeList = function(props) {
    const [shoes, setShoes] = useState([]);

    const getShoes = async function() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        const shoes = await response.json();
        setShoes(shoes);
    }

    const deleteShoe = async function(id) {
        const url = `http://localhost:8080/api/shoes/${id}/`

        try {
            const res = await fetch(url, {method: 'delete'})
            if (res.ok) {

                const newShoes = shoes.filter(shoe => shoe.id !== id);
                setShoes(newShoes);
            }
        } catch(e) {
            console.log('An error occurred deleting the hat', e);
        }
    }

    useEffect(() => {
        getShoes();
    },[])

    return(
        <>
            <h1>Shoes</h1>
            <div className="row">
                {shoes.map(shoe => <ShoeListItem key={shoe.id} shoe={shoe} deleteShoe={deleteShoe} />)}
            </div>
        </>

    )
}

export default ShoeList;
